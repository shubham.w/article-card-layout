
async function fetchDataAsync_fullname(url_fullName) {

    structured_data = [];
    //Getting Data from the API fetch
    const response = await fetch(url_fullName);
    var data = await response.json();
    // Saving data of objects in varaiables 
    // console.log(data)
    if (data.message){
        console.log(data.message);
    }else {
        console.log(data);
        return data;
    }
}



// single panel page javascript written from here and down 
async function loadNextpage (url){
    ///// Fetching country names in an array_countryName
    nextPanel_data = await fetchDataAsync_fullname(url);
    panelData = nextPanel_data[0];
    document.getElementById("default-img").src = panelData.flags.png;
    var navtiveName = document.getElementById("population");
    navtiveName.innerHTML = `Population : ${panelData.population}`;

   // const parent_div = document.getElementById("allrounder");
   // var div = document.createElement('div');
   // div.id = array_countryName[i].name.common;
   // div.innerHTML = `<img src="${array_countryName[i].flags.png}" alt="" class="flag-container"></img>`;
   // div.className = 'card-actual';
   // parent_div.appendChild(div);

   // const child_div = document.getElementById(array_countryName[i].name.common);
   // var inner_div = document.createElement('div');
   // inner_div.className = 'card-detail';
   // inner_div.id = `id-${array_countryName[i].name.common}`;
   // child_div.appendChild(inner_div);
}

//till here single panel javascript ends.

async function start_onload(){
    search_query = "portugal";
    // On Key press enter run this function for the search tab.
    fetchUrl = `https://restcountries.com/v3.1/name/${search_query}?fullText=true`;
    loadNextpage(fetchUrl);
}