//Global Variables
var tasks_todo = [];
// Onload Tasks
const getText =document.querySelector('#getText');
const content = document.getElementById("content");
var taskCounter;

const removeFromLocal = (removeTask)=> {
    console.log(tasks_todo);
    if (tasks_todo.length === 0){
        localStorage.setItem('taskData' , JSON.stringify(tasks_todo));
    }
    else{
        for (const ele of tasks_todo){
            if(ele.Id === removeTask){
                const index = tasks_todo.indexOf(ele);
                //Removing element from array perma remove.
                    tasks_todo.splice(index,1);  
                    console.log(tasks_todo);
                localStorage.setItem('taskData' , JSON.stringify(tasks_todo));
                
            }
        }
    }

}

function closeMe(destroyid){
    // Dynamic ID allocated//
    const destoryer = document.getElementById(`${destroyid}`);
        destoryer.style.display = "none";
    removeFromLocal(destroyid);   
}
function onEnter (task,status) {
    if(status === true){
        todo = "checked";
    }
    else{
        todo="unchecked";
    }
    const taskObj = {
        "Id" : `${taskCounter}`,
        "Task" : `${task}`,
        "Status" :`${todo}`  
    }
    tasks_todo.push(taskObj);
    console.log(tasks_todo);
// Convert data to stringify and save it in an array
    localStorage.setItem('taskData' , JSON.stringify(tasks_todo));
    content.innerHTML += `<div class="task-element" id="${taskCounter}">
        <input type="checkbox" id="checker" class="checker" ${tasks_todo[tasks_todo.length-1].Status}>
        <div class="data-fetch"><h4>${tasks_todo[tasks_todo.length-1].Task}</h4></div>
        <div class="delete-item">
        <button class="close-button" id="${taskCounter}"
        onclick="closeMe(this.id)">Close</button>
        </div>
        </div>`;
    taskCounter +=1;
    const inputTask = document.getElementById("getText");
        inputTask.value = "";
    const resetInput = document.getElementById("checked");
        resetInput.checked = false;
}

// FIlters  all,active,completed
function filterAll (){
    content.innerHTML = "";
    tasks_todo.forEach((element) => {
        console.log(element);
        content.innerHTML += `<div class="task-element" id="${element.Id}">
        <input type="checkbox" id="checker" class="checker" ${element.Status}>
        <div class="data-fetch"><h4>${element.Task}</h4></div>
        <div class="delete-item">
        <button class="close-button" id="${element.Id}"
        onclick="closeMe(this.id)">Close</button>
        </div>
        </div>`;
    })
}
function filterActive(){
    content.innerHTML = "";
    tasks_todo.forEach((element) => {
        if(element.Status === "unchecked"){
            console.log(element);
            content.innerHTML += `<div class="task-element" id="${element.Id}">
            <input type="checkbox" id="checker" class="checker" ${element.Status}>
            <div class="data-fetch"><h4>${element.Task}</h4></div>
            <div class="delete-item">
            <button class="close-button" id="${element.Id}"
            onclick="closeMe(this.id)">Close</button>
            </div>
            </div>`;
        }
    })
}
function filterCompleted(){
    content.innerHTML = "";
    tasks_todo.forEach((element) => {
        if(element.Status === "checked"){
            console.log(element);
            content.innerHTML += `<div class="task-element" id="${element.Id}">
            <input type="checkbox" id="checker" class="checker" ${element.Status}>
            <div class="data-fetch"><h4>${element.Task}</h4></div>
            <div class="delete-item">
            <button class="close-button" id="${element.Id}"
            onclick="closeMe(this.id)">Close</button>
            </div>
            </div>`;
        }
    })
}
function clearCompleted(){
    content.innerHTML = "";
    updated_task_todo = [];
    tasks_todo.forEach((element) => {
        if(element.Status === "unchecked"){
            console.log(element);
            content.innerHTML += `<div class="task-element" id="${element.Id}">
            <input type="checkbox" id="checker" class="checker" ${element.Status}>
            <div class="data-fetch"><h4>${element.Task}</h4></div>
            <div class="delete-item">
            <button class="close-button" id="${element.Id}"
            onclick="closeMe(this.id)">Close</button>
            </div>
            </div>`;
            updated_task_todo.push(element);
        }
    })
    localStorage.setItem('taskData', JSON.stringify(updated_task_todo));
}
function initLocalStorage (){
    console.log(tasks_todo);
    taskCounter = parseInt(tasks_todo[tasks_todo.length-1].Id)+1;
    // taskCounter = tasks_todo[tasks_todo.length-1].Id+1;
    // console.log(tasks_todo[tasks_todo.length-1].Id);
    tasks_todo.forEach((element,index) => {
        content.innerHTML += `<div class="task-element" id="${element.Id}">
        <input type="checkbox" id="checker" class="checker" ${element.Status}>
        <div class="data-fetch"><h4>${element.Task}</h4></div>
        <div class="delete-item">
        <button class="close-button" id="${element.Id}"
        onclick="closeMe(this.id)">Close</button>
        </div>
        </div>`;
    });
}

function onLoadPage (){
    // On Page body load do this 
    tasks_todo = JSON.parse(localStorage.getItem('taskData'));
    if((tasks_todo=== null) || (tasks_todo.length === 0)){
        tasks_todo = [];
        const taskObj = {
            "Id" : `0`,
            "Task" : `Welcome to TODO`,
            "Status" : `checked`
        }
        tasks_todo.push(taskObj);
        localStorage.setItem('taskData',JSON.stringify(tasks_todo));
        tasks_todo = JSON.parse(localStorage.getItem('taskData'));
        taskCounter = parseInt(tasks_todo[tasks_todo.length-1].Id)+1;
        tasks_todo.forEach((element,index) => { 
            content.innerHTML += `<div class="task-element" id="${element.Id}">
            <input type="checkbox" id="checker" class="checker" ${element.Status}>
            <div class="data-fetch"><h4>${element.Task}</h4></div>
            <div class="delete-item">
            <button class="close-button" id="${element.Id}"
            onclick="closeMe(this.id)">Close</button>
            </div>
            </div>`;
        });     
    }
    else{
        initLocalStorage();
    }
    // Event listeners on the Hopage here
    getText.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            const checked_status = document.getElementById("checked").checked;
            onEnter(getText.value,checked_status);
        }
    });
}