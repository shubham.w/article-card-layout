//Global Variables
var maxMoves = 10;
var arrayOfObject = [];
var liveTriggers = [];
var correctTriggers = [];
var count = 0;
var clickDisabled = false;

var uniqueId = [];
var uniqueColor =[];
//Event Listeners Global
const auto_render = document.getElementById("auto-render");
const moveCounter = document.getElementById("maxMoves");



const getUniqueIdArray = () => {
    // Returns a random integer from 1 to 8:
    var setOfId = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    return setOfId;
}

const getUniqueColor = () => {
    let maxVal = 0xFFFFFF; // 16777215 
    let randomNumber = Math.random() * maxVal; 
    randomNumber = Math.floor(randomNumber);
    randomNumber = randomNumber.toString(16);
    let randColor = randomNumber.padStart(6, 0);
    return `#${randColor.toUpperCase()}`;
}

const getUniqueColorArray = () =>{
    var setofColors = []
    for (let i =0;i<8;i++){
        setofColors.push(getUniqueColor());
    }
    duplicate = setofColors;
    var newSetofColors = setofColors.concat(duplicate);
    // console.log(newSetofColors);
    return newSetofColors;
}
function shuffle(array) {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
}
function checkRightWrong(triggersArr){
    const element1 = document.getElementById(triggersArr[0]);
    const element2 = document.getElementById(triggersArr[1]);
    // console.log(arrayOfObject[element1.id][1]);
    // console.log(arrayOfObject[element2.id][1]);
    // If answer is right
    if (arrayOfObject[element1.id][1] === arrayOfObject[element2.id][1]){
        console.log("Right Answer");
        return
    }
    // If answer is wrong
    else{ 
        maxMoves -= 1;
        console.log(maxMoves);
        moveCounter.innerHTML = `Moves Reamining: ${maxMoves}`;
        element1.style.backgroundColor = "rgb(204, 204, 204)";
        element2.style.backgroundColor = "rgb(204, 204, 204)";
        console.log("Wrong Answer");
        return
    }
}
const clickedThis = (click_id)=>{
    if (maxMoves <= 0){
        // Game Over
        moveCounter.innerHTML = `Game Over ( Better Luck Next Time)`;
    }else {
        if (count === 0){
            count += 1;
            liveTriggers.push(click_id);
            var changeColor = document.getElementById(click_id);
            changeColor.style.backgroundColor = `${arrayOfObject[click_id][1]}`
    
        }else if( count === 1){
            count =+ 1;
            liveTriggers.push(click_id);
            var changeColor = document.getElementById(click_id);
            
            changeColor.style.backgroundColor = `${arrayOfObject[click_id][1]}`
            setTimeout(()=> {
                changeColor.clickDisabled =true;
                checkRightWrong(liveTriggers);
                liveTriggers = [];
                count = 0;
            },300);
            
    
        }
    }
    

}

// Render ids and colors to div ///////////////////////
const makeGame = (set) => {
    // Parse inner HTML in auto-render id

    for (const i in set){
        console.log(set[i]);
        auto_render.innerHTML += 
        `<div class="unit-box" id="${set[i][0]}" 
        style="background-color: rgb(204, 204, 204)" 
        onclick="clickedThis(this.id)"> </div>`
    }
    
     // Add eventListener for two clicks on outerDiv.
    // console.log(auto_render);
    // for (var i= 0;i<auto_render.length;i++){  
    //     auto_render[i].addEventListener('click' , function(){
    //         document.getElementById("1").click();
    //         const clickedDiv = document.getElementById(click_id);
    //         clickedDiv.style.backgroundColor = `${uniqueColor[click_id]}`;
    //         console.log(uniqueColor[click_id]);
    //     });
    // }
}

const topLoad = () => {

    //  Get a unique pairs and place the IDs randomly.
    uniqueId = getUniqueIdArray();
    uniqueColor = shuffle(getUniqueColorArray());
    // Appending to single object key:value X8
    arrayOfObject = uniqueId.map(function (value, index){
        return [value, uniqueColor[index]]
    });
    // Append ids and colors to div
    makeGame(arrayOfObject); 

    
}

// `<div class="unit-box" id="${1}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${2}" onclick="turn(this.id,)"> </div>
//     <div class="unit-box" id="${3}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${4}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${5}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${6}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${7}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${8}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${9}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${10}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${11}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${12}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${13}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${14}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${15}" onclick="turn(this.id)"> </div>
//     <div class="unit-box" id="${16}" onclick="turn(this.id)"> </div>`;