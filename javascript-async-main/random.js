function fetchRandomNumbers(){
    console.log('Fetching number...');
    return new Promise((resolve , reject)=> {
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    });

}

function fetchRandomString(){
    console.log('Fetching string...');
    return new Promise ((resolve , reject)=> {
        setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
               result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);   
    });
    
}

// Using async Function
async function asyncCall(){
    console.log('Fetching async...');
    const fetched = await fetchRandomNumbers();
    const fetchedString = await fetchRandomString();
    console.log(fetched);
    console.log(fetchedString);
}
asyncCall();

//  Add random number to a sum integer
fetchRandomNumbers().then(outlog => {
    var sum = 30;
    console.log(outlog);
    sum += outlog;
    console.log(`Sum of ${outlog} + 30 is ${sum}`);
})

// Concat String and Number
fetchRandomString().then(outlog => {
    fetchRandomNumbers().then(output =>{
        console.log(outlog += output);
    });
})

// Add 10 random numbers
const addTenNumbers = () => {
    var sum =0;
    for (let i = 0;i< 10; i++){
        fetchRandomNumbers().then(outlog => {
            sum += outlog;
            console.log(sum);
            console.log(`Sum of ${outlog} is ${sum}`);
        })
    }
}
addTenNumbers();
