function counterFactory() {

// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.
var counter = 0;
object_inc_dec = {
    increment : function (){ 
        updatedCounter = counter +=1;
        return( updatedCounter);
    },
    decrement : function(){
        updatedCounter = counter -=1;
        return(updatedCounter);
    }
}
console.log(object_inc_dec.increment());

console.log(object_inc_dec.decrement());

}
counterFactory();


const callback = () => {
console.log("Hii");
return 0;
}
function limitFunctionCallCount(cb, n) {
// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned
if (n === 0) {
    return null;
}else{
    for (let i = 0; i<n;i++){
        cb();
    }
}

}
limitFunctionCallCount(callback,4);


const call = (n) => {
    for (let i = 0;i<n;i++)
    console.log("Hii");
    return 0;
    }
function cacheFunction(cb) {
// Should return a funciton that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.
var n =4;
cacheObj = {
    1 : 1,
    2: 3,
    3: 6
};
console.log(cacheObj);
for (const [ key , value] of Object.entries(cacheObj)){
    if (value === n){
        console.log(`Found in cache args ${n}`);
    }
    else {
        lastIndex = Object.keys(cacheObj)[Object.keys(cacheObj).length-1];
        cacheObj = {...cacheObj, lastIndex : n }
        console.log(`Notound in cache args ${n}`);
        console.log(`Hence passed to callBack ${n}`);
        cb(n);
        console.log(cacheObj);
    }
}



}

cacheFunction(call);