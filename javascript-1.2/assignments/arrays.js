

const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code. 
/*
  Complete the following functions.
  These functions only need to work with arrays.
  A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
  The idea here is to recreate the functions from scratch BUT if you'd like,
  feel free to Re-use any of your functions you build within your other functions.
  **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
  You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating
*/
function callBack(ret_val){
  alert(ret_val);
}

function each(elements, cb) {
  // var demo = Object.each(elements);
  // console.log(demo);
  // Do NOT use forEach to complete this function.
  // Iterates over a list of elements, yielding each in turn to the `cb` function.
  // This only needs to work with arrays.
  // You should also pass the index into `cb` as the second argument
  // based off http://underscorejs.org/#each
  for( const eachList of elements ){
    cb(eachList); 
  }
}
// each(items,callBack);



const call_back = (damn_you) => {
  damn_you += 3;
  console.log(damn_you);
  return (damn_you);
}
function map(elements, cb) {
  // Do NOT use .map, to complete this function.
  // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
  // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
  // Return the new array.
  var arr=[];
  for (let i = 0;i<elements.length; i++){
    arr.push(cb(elements[i]));
  }
  console.log(arr);

}
// map(items,call_back);
///////////////////////////////////
/////////////////////////////
/////////////////


const back_call = (start_val,ele) => {
  var combined_val;
  if(start_val === null){
    start_val = ele[0];
    combined_val = {};
    return combined_val;
  }
  else {
    combined_val = ele;
    return combined_val;
  }
}
function reduce(elements, cb, startingValue) {
  // Do NOT use .reduce to complete this function.
  // How reduce works: A reduce function combines all elements into a single value going from left to right.
  // Elements will be passed one by one into `cb` along with the `startingValue`.
  // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
  // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
  var show_val = [];
  if (startingValue === undefined) {
    for(let i = 0;i<elements.length; i++){
      show_val.push(cb(i, elements[i]));
      console.log(show_val);
    }
  }
  else {
    for(let i = startingValue;i<elements.length; i++){
      show_val.push(cb(i, elements[i]));
      console.log(show_val);
    }
  }
  
  console.log(typeof(show_val));
}
reduce(items,back_call,4 )
//////////////////////////////////
/////////////////////////////////

const backing_call = (myEle) =>{
  if (myEle == 2){
    return true;

  }
  else {
    return undefined;
  }
}
function find(elements, cb) {
  // Do NOT use .includes, to complete this function.
  // Look through each value in `elements` and pass each element to `cb`.
  // If `cb` returns `true` then return that element.
  // Return `undefined` if no elements pass the truth test.
  var show_val= [];
  for (const ele of elements) {
    if( cb(ele) == true){
      console.log(ele) ;
    }
    else {
      console.log(false) ; 
    }
  }
  console.log(show_val);
}
find(items,backing_call);

///////////////////////////////////
//////////////////////////////////
const back_calling = (myEle) =>{
  if (myEle == 2){
    return true;

  }
  else {
    return undefined;
  }
}
function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
  var show_val= [];
  for (const ele of elements) {
    show_val.push(cb(ele));
  }
  console.log(show_val);
}
filter(items,back_calling);

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
}
