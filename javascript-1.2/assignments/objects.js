// This assignment probably requires underscoreJS library :-p
// WIll do it after installation




const testObject = { "name": 'Bruce Wayne', "age": "36", "location": 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

function keys(obj) {
  // Retrieve all the names of the object's properties.
  console.log(obj.name);
  // Return the keys as strings in an array.
  var objKeys = Object.keys(obj);
  // Based on http://underscorejs.org/#keys
  console.log(objKeys);

}
keys(testObject);


function values(obj) {
  // Return all of the values of the object's own properties.
  var objValues = Object.values(obj);
  console.log(objValues);
  // Ignore functions
  // http://underscorejs.org/#values
}
values(testObject);

const transformFn = (return_val) => {
  return return_val += "1";

}
function mapObject(obj) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  var myObject;
  myObject = Object.values(obj).map(transformFn);
  console.log(myObject);
  // http://underscorejs.org/#mapObject
}
mapObject(testObject);

function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  var objPairs = Object.entries(obj);
  console.log(typeoJSON.stringify(objPairs));
  // http://underscorejs.org/#pairs
}
pairs(testObject);

/* STRETCH PROBLEMS */
function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // for (const [key, value] of reverse) {
  //   console.log(`${value} ${key}`);
  // }
  var newObj = {};
  for(keys in obj){
    newObj[obj[keys]] = keys;
    console.log(newObj);
  }

  // http://underscorejs.org/#invert
}
invert(testObject);

function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults

}
